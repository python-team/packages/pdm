pdm (2.22.1-1) UNRELEASED; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Build-Depends: Replace python3-pep517 by python3-pyproject-hooks
    Closes: #1043002
  * Build-Depends: python3-pdm-backend
  * Build-Depends: python3-pytest-httpserver to fix at least one test suite
    error
  * Fix import of BaseProvider
  * Provide artifacts in multi-source tarball to enable testing
  * Do not continue in case of failures since test artifacts are provided now
  * Drop .pdm-python

  [ eevelweezel ]
  * New upstream version.

  [ Stefano Rivera ]
  * Refresh patches.
  * Drop 0001-Disable-dynamic-version.patch, no longer needed.
  * Remove missing-sources, no longer needed.

  [ eevelweezel ]
  * Standards-Version: 4.7.0 (routine-update)
  * New upstream version.

 -- eevelweezel <eevel.weezel@gmail.com>  Wed, 8 Jan 2025 18:37:12 -0500

pdm (2.2.1+ds1-1) unstable; urgency=medium

  * New upstream release 2.2.1.

 -- Boyuan Yang <byang@debian.org>  Sat, 26 Nov 2022 15:15:11 -0500

pdm (2.1.4+ds1-1) unstable; urgency=medium

  * New upstream release 2.1.4.
  * debian/control: Update package version requirement.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Wed, 21 Sep 2022 14:24:36 -0400

pdm (2.0.3+ds1-1) unstable; urgency=medium

  * New upstream release 2.0.3.
  * debian/control: Update package version requirement.
  * d/p/0002-disable-test_integration: Dropped, not needed anymore.

 -- Boyuan Yang <byang@debian.org>  Sat, 23 Jul 2022 17:16:45 -0400

pdm (2.0.1+ds1-1) unstable; urgency=medium

  * New upstream release 2.0.1.
  * debian/rules: Disable using dynamic version for now to correct pdm
    version information.
  * debian/patches: Add patch to disable test_integration. These tests
    needs python3 versions outside of Debian's support scope.

 -- Boyuan Yang <byang@debian.org>  Sun, 17 Jul 2022 11:58:39 -0400

pdm (2.0.0b2+ds1-1) unstable; urgency=medium

  * New upstream version 2.0.0b2+ds1.
  * debian/: Apply wrap-and-sort -abst.
  * Bump version requirement for python3-pdm-pep517 and python3-unearth.

 -- Boyuan Yang <byang@debian.org>  Sat, 09 Jul 2022 15:24:57 -0400

pdm (2.0.0b1+ds1-1) unstable; urgency=medium

  * Initial release. (Closes: #1014067)

 -- Boyuan Yang <byang@debian.org>  Sat, 02 Jul 2022 17:53:05 -0400
